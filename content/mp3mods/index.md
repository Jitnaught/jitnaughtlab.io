+++
title = 'My Max Payne 3 mods'
date = 2024-08-20T12:25:58-07:00
draft = false
+++

# My Max Payne 3 mods

{{< download name="AutoShoot" desc="Automatically shoot at nearby enemies" image="https://i.imgur.com/9X5fTT5.jpg" download="https://mega.nz/file/NbRijZ6a#Xfup267O2s-cNLg00oG0y5DsymrA5D_eDnVXHQcSwJ4" >}}

{{< download name="Disarm" desc="Shoot at enemies hand and they'll drop their weapon" image="https://i.imgur.com/ZJP0nLv.jpg" download="https://mega.nz/file/QHJgiRrK#o6tWHu1IJQj3WDp59K1ynk9xfrmLEIghBX-aWHXOCAg" >}}

{{< download name="Forcefield" desc="Push cars and peds away" image="https://i.imgur.com/v3AZRRP.jpg" download="https://mega.nz/file/EXB2XJyZ#e3DSPPh9DSA9BVT3CrMNkG43t00UDrRccUNX3pDGvx8" >}}

{{< download name="Shoot Cars" desc="Bullets are cars" image="https://i.imgur.com/V9XUXwx.jpg" download="https://mega.nz/file/UXImAJ4Q#DZ6w2Rj3i1zyOPEi0rb6csnnuK5ZaZ73H1S8UKh45oE" >}}

{{< download name="Weird Rain" desc="Rains cars" image="https://i.imgur.com/P8gITTe.jpg" download="https://mega.nz/file/BGYWkL7T#1RAmPx-lRlnJVYXcip-U6bW4bmYOkcAZsmDKWNiBvsY" >}}