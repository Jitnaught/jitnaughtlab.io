+++
title = 'My GTA 4 bookmarks'
date = 2024-11-18T03:18:52-07:00
draft = false
+++

# My GTA 4 documentation bookmarks

A lot of the documentation here could be outdated.

{{< bookmark title="GTA4-Mods archive" link="https://web.archive.org/web/20170520221731/http://www.gta4-mods.com/" desc="THE spot for GTA 4 mods, shut down after hack. At time of archive, site had been dead a while, since rappo left" >}}

{{< bookmark title="GTAGaming shutdown notice" link="https://web.archive.org/web/20170531023801/https://www.gta4-mods.com/" desc="GTAGaming ran GTA4-Mods" >}}

{{< bookmark title="GTAInside" link="https://www.gtainside.com/en/gta4mods" desc="Was less popular site for hosting mods" >}}

{{< bookmark title="GTAGarage" link="https://www.gtagarage.com/mods/browse.php?C=0&State=0&Type=0&Game=6&Order=Mod_LastUpdate&Dir=%20DESC&Thumbs=0&st=1875" desc="Don't know if anybody really used this one" >}}

{{< bookmark title="IV-MP T4" link="https://iv-mp.eu/" desc="Alternative MP" >}}

{{< bookmark title="IV:MP archive" link="https://web.archive.org/web/20200218145524/http://gta-ivmp.com/" desc="Alternative MP" >}}

{{< bookmark title="Recommended 1080 mods" link="https://gtaforums.com/topic/954500-recommended-mods-for-1080-and-above/" desc="" >}}

{{< bookmark title="Modding guide" link="https://gist.github.com/Juansero29/048148c8e695aba9c59e4189a1cbf862#net-mods-and-gta-4-net-scripthook" desc="" >}}

{{< bookmark title="Compatibility patch for 1080" link="https://www.lcpdfr.com/downloads/gta4mods/g17media/26726-compatibility-patch-for-gta-iv-complete-edition/" desc="" >}}

{{< bookmark title="Modding fix" link="https://steamcommunity.com/app/12210/discussions/0/154642447912806392/" desc="Patch 1080 broke scripthook, guide to downgrade" >}}

{{< bookmark title="Ultimate ASI Loader" link="https://github.com/ThirteenAG/Ultimate-ASI-Loader/releases" desc="" >}}

{{< bookmark title="1080 scripthook" link="https://github.com/themonthofjune/IV.1080.ScriptHook" desc="Don't know if it works" >}}

{{< bookmark title="ScriptHook .NET updated" link="https://github.com/Tomasak/gta4_scripthookdotnet/releases" desc="Rain bug fixed, probably updated for 1080" >}}

{{< bookmark title="Zolika trainer" link="https://gtaforums.com/topic/896795-1000-1080-zolika1351s-trainermod-menu-rewritten/" desc="A recent trainer" >}}

{{< bookmark title="Zolika's mods" link="https://zolika.dev/games/gtaiv" desc="Bunch of fixes, improvements, a trainer, etc" >}}

{{< bookmark title="XLiveLess" link="https://github.com/jforkster/xliveless" desc="" >}}

{{< bookmark title="YAASIL - ASI loader" link="https://hazardx.com/files/gta4_yaasil-84" desc="" >}}

{{< bookmark title="ScriptHook .NET 1080 support" link="https://gtaforums.com/topic/946154-release-gtaiv-net-scripthook-v1718-support-for-gta-iv-1080-and-eflc-1130-by-arinc9-zolika1351/" desc="1080 support" >}}

{{< bookmark title="ScriptHook .NET" link="https://github.com/HazardX/gta4_scripthookdotnet" desc="" >}}

{{< bookmark title="Aru's scripthook" link="http://www.dev-c.com/gtaiv/scripthook/" desc="" >}}

{{< bookmark title="List of native functions" link="https://gtamods.com/wiki/List_of_native_functions_(GTA_IV)" desc="" >}}

{{< bookmark title="Native functions list" link="https://web.archive.org/web/20221204090112/https://pastebin.com/n7CAnpZB" desc="Probably outdated" >}}

{{< bookmark title="Natives" link="https://public.sannybuilder.com/GTA4/gta_natives_pc.txt" desc="Probably outdated" >}}

{{< bookmark title="Natives" link="https://public.sannybuilder.com/GTA4/natives.txt" desc="Lists number of parameters and returns. Probably outdated" >}}

{{< bookmark title="ivmultiplayer native functions" link="https://github.com/Neproify/ivmultiplayer/blob/master/Client/Core/Scripting.h" desc="" >}}

{{< bookmark title="ivmultiplayer source" link="https://code.google.com/archive/p/ivmultiplayer/source/default/source" desc="" >}}

{{< bookmark title="Draw text inside world" link="https://gtaforums.com/topic/667263-draw-text-inside-world/" desc="GTAForums thread" >}}

{{< bookmark title="Draw inside world" link="https://pastebin.com/BG77rMQm" desc="ScriptHook .NET code" >}}

{{< bookmark title="How to use ScriptHook C++" link="https://gtaforums.com/topic/576633-how-to-use-arus-c-scripthook-sdk/" desc="GTAForums thread" >}}

{{< bookmark title="Scripting tutorials" link="https://gtaxscripting.blogspot.com/p/tutorials.html" desc="By JulioNIB" >}}

{{< bookmark title="Writing scripts in .NET/C#" link="https://gtaforums.com/topic/401577-iv-net-writing-net-scripts-in-c/" desc="ScriptHook .NET" >}}

{{< bookmark title="Gamemodes" link="https://gtamods.com/wiki/List_of_Gamemodes_(GTA4)" desc="For multiplayer, which has been shutdown (GFWL)" >}}

{{< bookmark title="Particle effects" link="https://gtamods.com/wiki/List_of_particle_effects" desc="" >}}

{{< bookmark title="Weapons" link="https://gtamods.com/wiki/List_of_Weapons_(GTA4)" desc="" >}}

{{< bookmark title="Ped bones" link="https://gtamods.com/wiki/Ped_Bones" desc="" >}}

{{< bookmark title="Control codes" link="https://i.imgur.com/8PqPQeL.png" desc="No idea why it's a screenshot, I probably uploaded this back then" >}}

{{< bookmark title="Blips" link="https://public.sannybuilder.com/GTA4/blips/" desc="Has images" >}}

{{< bookmark title="Text colors, blips, and buttons" link="https://web.archive.org/web/20130426015112/http://psx-scene.com/forums/f312/customize-strings-colors-blips-other-things-107029" desc="" >}}

{{< bookmark title="Animations" link="https://openiv.com/public/Animations.txt" desc="" >}}

{{< bookmark title="Some animations" link="https://pastebin.com/74hyGLDG" desc="" >}}

{{< bookmark title="Animation flag definitions ScriptHook .NET" link="https://pastebin.com/ebbbDtSX" desc="LetsPlayOrDy is my old handle" >}}

{{< bookmark title="Animations" link="https://public.sannybuilder.com/anim/GTAIV_Animations.txt" desc="" >}}

{{< bookmark title="List of ped models" link="https://pastebin.com/zHUxUrYc" desc="" >}}

{{< bookmark title="List of model hashes" link="https://gtamods.com/wiki/List_of_models_hashes" desc="" >}}

{{< bookmark title="Cool and useful object hashes" link="https://www.se7ensins.com/forums/threads/lots-of-cool-and-useful-gta-iv-object-hashes.807623/" desc="" >}}

{{< bookmark title="Vehicle source directory" link="https://gtaforums.com/topic/503904-iv-vehicle-sound-directory/" desc="" >}}

{{< bookmark title="Cars IDE" link="https://gtamods.com/wiki/CARS_(IDE_Section)#GTA_IV" desc="" >}}

{{< bookmark title="carcols.dat" link="https://gtamods.com/wiki/Carcols.dat#GTA_IV" desc="Car colors" >}}

{{< bookmark title="Function memory addresses" link="https://gtamods.com/wiki/Function_Memory_Addresses_(IV)" desc="" >}}

{{< bookmark title="Memory addresses" link="https://gtamods.com/wiki/Memory_Addresses_(GTA4)" desc="" >}}

{{< bookmark title="Pools" link="https://web.archive.org/web/20081210095353/http://public.yoa2n.fr:80/gtaiv/Pools.txt" desc="'Arrays' of stuff stored in memory, like entities and such" >}}

{{< bookmark title="Classes" link="https://web.archive.org/web/20081210092317/http://public.yoa2n.fr:80/gtaiv/Documentation.txt" desc="" >}}

{{< bookmark title="native.idc" link="https://public.sannybuilder.com/GTA4/native.idc" desc="" >}}

{{< bookmark title="PC classes" link="https://public.sannybuilder.com/GTA4/gta4_pc_classes.txt" desc="" >}}

{{< bookmark title="RSC" link="https://public.sannybuilder.com/GTA4/rsc_en.txt" desc="Dunno what this is, above my pay grade" >}}

{{< bookmark title="Native template" link="https://gtamods.com/wiki/Template:Native" desc="GTAModding wiki editing" >}}

{{< bookmark title="NOP function" link="https://pastebin.com/DEQFRvtP" desc="GTAModding wiki editing" >}}
