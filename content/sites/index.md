+++
title = 'My websites'
date = 2024-08-19T11:00:30-07:00
draft = false
+++

# My websites

{{< site name="RDR2Modding" desc="Host mods for RDR2, defunct" image="https://i.imgur.com/PsqKnTA.jpg" link="https://web.archive.org/web/20191106195847/https://rdr2modding.com/" >}}

{{< site name="fosmods" desc="Host free and open-source mods, defunct" image="https://i.imgur.com/tq5nGwa.jpg" link="https://web.archive.org/web/20220216184102/https://www.fosmods.com/" >}}