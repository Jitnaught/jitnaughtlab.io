+++
title = 'My GTA 5 bookmarks'
date = 2024-11-18T01:09:30-07:00
draft = false
+++

# My GTA 5 documentation bookmarks

A lot of the documentation here could be outdated.

{{< bookmark title="dotindustries NativeDB" link="https://nativedb.dotindustries.dev/" desc="Easy to search" >}}

{{< bookmark title="alloc8or NativeDB" link="https://alloc8or.re/gta5/nativedb/" desc="THE source for native docs. dotindustries uses data from here" >}}

{{< bookmark title="FiveM NativeDB" link="https://runtime.fivem.net/doc/natives/" desc="Another source for natives. Has FiveM specific natives as well" >}}

{{< bookmark title="Alexander Blade NativeDB" link="http://dev-c.com/nativedb/" desc="The OG native docs. Anyone can edit the docs. Not as good as alloc8or or dotindustries" >}}

{{< bookmark title="OpenVHook" link="https://github.com/SaneEngineer/OpenVHook" desc="Open-source ScriptHookV alternative" >}}

{{< bookmark title="ScriptHookVDotNet source" link="https://github.com/scripthookvdotnet/scripthookvdotnet/tree/main/source" desc="Useful for finding how certain functions and natives work" >}}

{{< bookmark title="Command line options" link="https://support.rockstargames.com/articles/202518358/Full-list-of-command-line-parameters-for-Grand-Theft-Auto-V-on-PC" desc="Parameters for GTA 5" >}}

{{< bookmark title="Script/Native Documentation and Research" link="http://gtaforums.com/topic/717612-v-scriptnative-documentation-and-research/" desc="Thread started by Alexander Blade" >}}

{{< bookmark title="GTAForums documentation" link="http://gtaforums.com/forum/371-documentation/" desc="Documentation subforum on GTAForums" >}}

{{< bookmark title="RAGEPluginHook documentation" link="https://docs.ragepluginhook.net/" desc="API docs for RPH" >}}

{{< bookmark title="WikiFive" link="https://github.com/Konijima/WikiFive" desc="Lots of various docs" >}}

{{< bookmark title="UnknownModder pastebin archive" link="https://web.archive.org/web/20210714225459/https://pastebin.com/u/UnknownModder" desc="alloc8or's old pastebin (his original name was UnknownModder)" >}}

{{< bookmark title="RAGE Parser Dumps" link="https://alexguirre.github.io/rage-parser-dumps/" desc="Dumped structs" >}}

{{< bookmark title="Data dumps" link="https://github.com/DurtyFree/gta-v-data-dumps" desc="Extracted data dumps" >}}

{{< bookmark title="Decompiled scripts" link="https://github.com/root-cause/v-decompiled-scripts" desc="Internal GTA 5 scripts decompiled" >}}

{{< bookmark title="How to decompile scripts" link="https://pastebin.com/wXCVizRm" desc="Instructions" >}}

{{< bookmark title="Class names" link="https://pastebin.com/PV2jeB7P" desc="Internal class names" >}}

{{< bookmark title="How to get speed limit" link="https://gtaforums.com/topic/880868-how-to-get-the-speed-limit-of-the-streetroadfreeway/" desc="Forum thread" >}}

{{< bookmark title="Get speed limit .NET" link="https://pastebin.com/a2kLF7HB" desc="Code I decompiled from LSPDFR mod" >}}

{{< bookmark title="NaturalMotion options" link="https://pastebin.com/RxJc0jaj" desc="IIRC these are bit flags" >}}

{{< bookmark title="HTML" link="https://pastebin.com/jvCBUfLz" desc="Using HTML for styling and images" >}}

{{< bookmark title="Timecycle modifiers" link="https://web.archive.org/web/20180312154138/https://pastebin.com/kVPwMemE" desc="From UM's pastebin" >}}

{{< bookmark title="Radio stations" link="https://web.archive.org/web/20211024160434/https://pastebin.com/Kj9t38KF" desc="From UM's pastebin" >}}

{{< bookmark title="Markers" link="https://web.archive.org/web/20180724015529/https://wiki.gtanet.work/index.php?title=Marker" desc="Used to have images that were useful" >}}

{{< bookmark title="Explosion types" link="https://web.archive.org/web/20220805133301/https://wiki.gtanet.work/index.php?title=Explosions" desc="" >}}

{{< bookmark title="Weather types" link="https://web.archive.org/web/20230519101447/https://wiki.gtanet.work/index.php?title=Weather" desc="Has images" >}}

{{< bookmark title="Door types" link="https://web.archive.org/web/20230331230545/https://wiki.gtanet.work/index.php?title=Doors" desc="Has images" >}}

{{< bookmark title="Cutscenes list" link="https://pastebin.com/DmCkxuQE" desc="Probably outdated" >}}

{{< bookmark title="Scenarios list" link="https://pastebin.com/Apy9n9Kq" desc="Probably outdated" >}}

{{< bookmark title="Relationship groups" link="https://gtaforums.com/topic/793324-relationship-groups-stopping-random-peds-from-fleeing/#comment-1067529477" desc="How to use relationship groups" >}}

{{< bookmark title="Formations" link="https://pastebin.com/AFr1ury1" desc="Formations for groups" >}}

{{< bookmark title="Mobile phone types" link="https://pastebin.com/fB65DgcB" desc="" >}}

{{< bookmark title="Shake cam types" link="https://pastebin.com/Tehz59FK" desc="" >}}

{{< bookmark title="Notification pictures" link="https://pastebin.com/mFW3k0jV" desc="Probably outdated" >}}

{{< bookmark title="Radio names" link="https://pastebin.com/VcpwzM1c" desc="Outdated" >}}

{{< bookmark title="Tunables" link="https://pastebin.com/SKJ0FcnJ" desc="Old multiplayer things" >}}

{{< bookmark title="Paths" link="https://gta.fandom.com/wiki/Paths_(GTA_V)" desc="Bit flags for nodes" >}}

{{< bookmark title="Rockstar dev shirts" link="https://www.unknowncheats.me/forum/grand-theft-auto-v/576746-unlock-rockstar-developer-shirts-using-admin-dlc.html" desc="Admin DLC" >}}

{{< bookmark title="Get hover transform ratio" link="https://gtaforums.com/topic/954374-get-if-deluxo-is-currently-in-hover-mode/?tab=comments#comment-1071206741" desc="Deluxo" >}}

{{< bookmark title="Head blend data" link="https://gist.github.com/ikt32/466c59df7aff69d2ac788fa38e669300" desc="Get unsafe struct from native function, head blend data" >}}

{{< bookmark title="Look Rotation method like Unity's" link="https://pastebin.com/XJ74H2th" desc="Gets the rotation needed to face the specified direction" >}}

{{< bookmark title="GameMath.cs" link="https://github.com/citizenfx/fivem/blob/master/code/client/clrcore/Math/GameMath.cs" desc="Some useful functions" >}}

{{< bookmark title="Lock/unlock decorators" link="https://pastebin.com/XXwqNNQ1" desc="For saving data on entities" >}}

{{< bookmark title="Vehicle node flags" link="https://pastebin.com/Dk8LNSCc" desc="Get road flags" >}}

{{< bookmark title="DISPLAY_ONSCRCREEN_KEYBOARD" link="https://gtaforums.com/topic/717612-v-scriptnative-documentation-and-research/?do=findComment&comment=1067396160" desc="" >}}

{{< bookmark title="Clear cheating reports and bad behavior" link="https://www.mpgh.net/forum/showthread.php?t=950339" desc="OLD multiplayer script" >}}

{{< bookmark title="Menu base SPRX" link="https://web.archive.org/web/20150405120843/http://topusefulsolutions.com/13264/sprx-c-menu-base-source" desc="" >}}

{{< bookmark title="Call native by hash" link="https://pastebin.com/Z3VxWMMD" desc="Creating your own script hook" >}}

{{< bookmark title="Classes list" link="https://gta.nick7.com/stuff/gta5/ps3-classes.txt" desc="From PS3" >}}

{{< bookmark title="FindPattern C++" link="https://github.com/ikt32/GTAVAddonLoader/blob/master/GTAVAddonLoader/NativeMemory.cpp#L145" desc="For memory reading and manipulation" >}}

{{< bookmark title="Entity types" link="https://web.archive.org/web/20220805133316/https://wiki.gtanet.work/index.php?title=Entity_Types" desc="" >}}

{{< bookmark title="Bone indices" link="https://pastebin.com/D7JMnX1g" desc="" >}}

{{< bookmark title="Clothing and props" link="https://wiki.rage.mp/index.php?title=Clothes" desc="" >}}

{{< bookmark title="Ped components and prop variations" link="https://gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html" desc="" >}}

{{< bookmark title="Combat behavior flags" link="https://gtaforums.com/topic/833391-researchguide-combat-behaviour-flags/" desc="" >}}

{{< bookmark title="combatbehavior.meta" link="https://pastebin.com/Jmcd6rFR" desc="Internal data on combat behavior flags" >}}

{{< bookmark title="CombatFlag enum" link="https://pastebin.com/MvE3Akam" desc="All of the combat flags I could find" >}}

{{< bookmark title="Tasks" link="https://pastebin.com/EirNYfrc" desc="GET_IS_TASK_ACTIVE" >}}

{{< bookmark title="Task IDs" link="https://pastebin.com/2gFqJ3Px" desc="CTask" >}}

{{< bookmark title="Task hashes" link="https://pastebin.com/R9iK6M9W" desc="GET_SCRIPT_TASK_STATUS" >}}

{{< bookmark title="Ped subtasks" link="https://gtaforums.com/topic/854145-ped-subtasks-knowing-if-a-ped-is-doing-something-specific/#entry1068754396" desc="" >}}

{{< bookmark title="Car mods" link="https://pastebin.com/QzEAn02v" desc="" >}}

{{< bookmark title="Driving styles" link="https://gtaforums.com/topic/822314-guide-driving-styles/" desc="" >}}

{{< bookmark title="Vehicle color IDs" link="https://gtamods.com/wiki/Vehicle_Color_IDs" desc="" >}}

{{< bookmark title="Best upgraded cars" link="https://web.archive.org/web/20150422201419/https://docs.google.com/spreadsheets/d/t7tvIBuwN9gYN3S7uOP7tZQ/htmlview?pli=1" desc="Very outdated" >}}

{{< bookmark title="Driving style bit flags" link="https://pastebin.com/dSVzEgbM" desc="" >}}

{{< bookmark title="Hash list" link="https://web.archive.org/web/20150607011400/http://www.gta5-mystery-busters.onet.domains/tools/hashlist.php" desc="Outdated list of models YDR, YFT and YBN" >}}

{{< bookmark title="Prop list" link="https://gist.githubusercontent.com/leonardosnt/53faac01a38fc94505e9/raw/efb6eb289a6c29c0ba7f5aba6005cda081f3928b/GTA%2520V%2520PROP%2520LIST" desc="Outdated" >}}

{{< bookmark title="Object gallery" link="https://web.archive.org/web/20180318165137/http://objects.codeshock.hu:80/gallery.php" desc="Pictures of objects (outdated)" >}}

{{< bookmark title="Peds" link="https://web.archive.org/web/20230422145058/https://wiki.gtanet.work/index.php?title=Peds" desc="Has images, outdated" >}}

{{< bookmark title="Vehicle models" link="https://web.archive.org/web/20230514212045/https://wiki.gtanet.work/index.php?title=Vehicle_Models" desc="Has images, outdated" >}}

{{< bookmark title="Peds & hashes" link="https://pastebin.com/gDgCcJua" desc="Outdated" >}}

{{< bookmark title="Object list" link="https://pastebin.com/6cCmTJk0" desc="Outdated" >}}

{{< bookmark title="Pickups" link="https://web.archive.org/web/20220805133318/https://wiki.gtanet.work/index.php?title=Pickups" desc="Outdated" >}}

{{< bookmark title="Particles" link="https://particles.altv.mp/" desc="Has gifs" >}}

{{< bookmark title="PTFX asset names" link="https://pastebin.com/zaxRrFwp" desc="Probably outdated" >}}

{{< bookmark title="Entity FX" link="https://pastebin.com/wq4GKR5M" desc="" >}}

{{< bookmark title="Particle FXs" link="https://pastebin.com/N9unUFWY" desc="Probably outdated" >}}

{{< bookmark title="Screen FX" link="https://pastebin.com/Wf5RphKU" desc="Probably outdated" >}}

{{< bookmark title="Particle effects dump" link="https://gist.github.com/alexguirre/af70f0122957f005a5c12bef2618a786" desc="Seems up-to-date" >}}

{{< bookmark title="Particle effects" link="https://pastebin.com/SME7bKaS" desc="Probably outdated" >}}

{{< bookmark title="Particles list" link="https://web.archive.org/web/20180106165000/https://devtesting.pizza/particle-list/" desc="Probably outdated" >}}

{{< bookmark title="List of speeches" link="https://pastebin.com/1GZS5dCL" desc="Outdated" >}}

{{< bookmark title="Speeches" link="https://pastebin.com/Vk9609qj" desc="Outdated" >}}

{{< bookmark title="RAGEPluginHook speeches" link="https://gist.githubusercontent.com/alexguirre/0af600eb3d4c91ad4f900120a63b8992/raw/29854f8c94a54d020db5b0d2c0d218dd57432894/Speeches.cs" desc="" >}}

{{< bookmark title="Blip sprite ids" link="https://gtamods.com/wiki/Blip_Sprite_IDs" desc="" >}}

{{< bookmark title="Blip sprites" link="https://web.archive.org/web/20221027213240/https://wiki.gtanet.work/index.php?title=Blips" desc="With images, probably outdated" >}}

{{< bookmark title="IPLs" link="https://pastebin.com/42JDPZBr" desc="Outdated" >}}

{{< bookmark title="IPL names" link="https://pastebin.com/pwkh0uRP" desc="Outdated" >}}

{{< bookmark title="Online interiors and locations" link="https://web.archive.org/web/20230517214626/https://wiki.gtanet.work/index.php?title=Online_Interiors_and_locations" desc="Outdated" >}}

{{< bookmark title="Controls" link="https://github.com/scripthookvdotnet/scripthookvdotnet/blob/main/source/scripting_v3/GTA/Control.cs" desc="" >}}

{{< bookmark title="Colors and button inputs" link="https://pastebin.com/nqNYWMSB" desc="For use in text" >}}

{{< bookmark title="Input names" link="https://pastebin.com/LbkTK5WX" desc="" >}}

{{< bookmark title="Police radio audio" link="https://pastebin.com/1CSnch40" desc="" >}}

{{< bookmark title="Sound effects" link="https://pastebin.com/YUj3Pugq" desc="Probably outdated" >}}

{{< bookmark title="Audio list" link="https://pastebin.com/JCdbeJkk" desc="Probably outdated" >}}

{{< bookmark title="FrontEndSoundList" link="https://web.archive.org/web/20220805133301/https://wiki.gtanet.work/index.php?title=FrontEndSoundlist" desc="Probably outdated" >}}

{{< bookmark title="Use HTML, images, and blips" link="https://forum.cfx.re/t/using-html-images-and-blips-in-scaleform-texts/553298" desc="For scaleforms and text" >}}

{{< bookmark title="Fonts and colors" link="https://pastebin.com/V0yJ6QS0" desc="" >}}

{{< bookmark title="Colors" link="https://web.archive.org/web/20150907174557/http://pastie.org/private/so6mb8qpmwundzzj9pskda" desc="" >}}

{{< bookmark title="Fonts" link="https://web.archive.org/web/20220805133305/https://wiki.gtanet.work/index.php?title=Fonts" desc="" >}}

{{< bookmark title="Weapons" link="https://wiki.rage.mp/index.php?title=Weapons" desc="Has images, probably outdated" >}}

{{< bookmark title="Weapon components" link="https://pastebin.com/mrsApNav" desc="Outdated" >}}

{{< bookmark title="Weapon models" link="https://web.archive.org/web/20230402152650/https://wiki.gtanet.work/index.php?title=Weapons_Models" desc="Outdated" >}}

{{< bookmark title="Weapon components" link="https://web.archive.org/web/20220805133311/https://wiki.gtanet.work/index.php?title=Weapons_Components" desc="Outdated" >}}

{{< bookmark title="Weapon tints" link="https://web.archive.org/web/20220805133307/https://wiki.gtanet.work/index.php?title=Weapons_Tints" desc="" >}}

{{< bookmark title="Ammo types" link="https://gist.github.com/root-cause/faf41f59f7a6d818b7db0b839bd147c1" desc="Outdated" >}}

{{< bookmark title="Decompiled scaleforms" link="https://vespura.com/fivem/scaleform/#" desc="Maybe outdated" >}}

{{< bookmark title="Scaleform functions" link="https://web.archive.org/web/20190209184747/http://scaleform.devtesting.pizza/" desc="Probably outdated" >}}

{{< bookmark title="Scaleform minimap functions" link="https://web.archive.org/web/20180106164726/https://scaleform.devtesting.pizza/minimap.html" desc="Probably outdated" >}}

{{< bookmark title="Sprite3D" link="https://gist.github.com/alexguirre/e6b8dc7cdbee8b3ec5227a056a7ce535#file-sprite3d-cs-L46" desc="" >}}

{{< bookmark title="Sprite list" link="https://pastebin.com/R5AbQJqb" desc="Probably outdated" >}}

{{< bookmark title="Animations list" link="https://alexguirre.github.io/animations-list/" desc="Probably outdated" >}}

{{< bookmark title="List of anims" link="https://web.archive.org/web/20150701012913/http://www.gta5-mystery-busters.onet.domains/tools/anims.php" desc="Outdated" >}}

{{< bookmark title="Animations list" link="https://docs.ragepluginhook.net/html/62951c37-a440-478c-b389-c471230ddfc5.htm" desc="Probably outdated" >}}

{{< bookmark title="Animations" link="https://web.archive.org/web/20220805133316/https://wiki.gtanet.work/index.php?title=Animations" desc="Outdated" >}}

{{< bookmark title="Relaxed sitting animation" link="https://pastebin.com/Vg6zDLUw" desc="" >}}

{{< bookmark title="jedijosh920 Strapped mod decompile" link="https://pastebin.com/Psn9jth9" desc="" >}}

{{< bookmark title="RAGE Multiplayer" link="https://wiki.rage.mp/index.php?title=Main_Page" desc="Alternative MP docs" >}}

{{< bookmark title="GTA Network" link="https://web.archive.org/web/20220805133303/https://wiki.gtanet.work/index.php?title=Category:Reference" desc="Alternative MP docs" >}}

{{< bookmark title="Mod requests reddit" link="https://old.reddit.com/r/GTAV_Mods/search?sort=new&restrict_sr=on&q=flair%3ARequest" desc="" >}}

{{< bookmark title="Mod requests GTAForums" link="https://gtaforums.com/topic/806125-gta-v-modding-requests-finds/" desc="" >}}

{{< bookmark title="LeeC2202 modding" link="https://archive.is/S7yi7" desc="He removed most of his content off GTA5-Mods, GTAForums, and delisted his site from the Wayback Machine. Unrelated, he didn't like me decompiling and trying to fix one of his mods" >}}

{{< bookmark title="dnSpyEx" link="https://github.com/dnSpyEx/dnSpy" desc="Program for decompiling .NET assemblies, such as ScriptHookVDotNet mods" >}}
