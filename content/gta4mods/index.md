+++
title = 'My GTA IV mods'
date = 2024-08-19T02:20:22-07:00
draft = false
+++

# My GTA IV mods

[Archive of GTA4-Mods profile](https://web.archive.org/web/20170316164657/http://www.gtagaming.com/downloads/author/207250)

[Older archive](https://web.archive.org/web/20131013153715/http://www.gtagaming.com:80/downloads/author/207250) (has some old deleted mods)


{{< download name="AmphbiousVehicles" desc="Set up vehicles so they can \"float\" in water" image="https://i.imgur.com/bYoHe1f.jpg" download="https://www.gtagaming.com/amphibiousvehicles-f33255.html" >}}

{{< download name="V Style Kill Marker" desc="GTA V style kill marker" image="https://i.imgur.com/wnhfYGW.jpg" download="https://www.gtagaming.com/v-style-kill-marker-f31984.html" >}}

{{< download name="Mass Suicide" desc="Everyone nearby shoots themselves" image="https://i.imgur.com/y54HJO0.jpg" download="https://www.gtagaming.com/mass-suicide-f31812.html" >}}

{{< download name="Speed Limit" desc="Get wanted stars if cops catch you speeding" image="https://i.imgur.com/7rELnbp.jpg" download="https://www.gtagaming.com/speed-limit-f31572.html" >}}

{{< download name="Top Down View" desc="GTA 2 style camera" image="https://i.imgur.com/x28OBw2.jpg" download="https://www.gtagaming.com/top-down-view-gta-i-ii-style-f31530.html" >}}

{{< download name="DOS IV" desc="Fake terminal/command line" image="https://i.imgur.com/yEzf2E7.jpg" download="https://www.gtagaming.com/dos-iv-0-9-beta-f31438.html" >}}

{{< download name="Explosive Rounds" desc="Bullets explode" image="https://i.imgur.com/3mqfieJ.jpg" download="https://www.gtagaming.com/explosive-rounds-f31364.html" >}}

{{< download name="RealWeather" desc="Real life weather becomes in-game weather" image="https://i.imgur.com/NgtV5Pm.jpg" download="https://www.gtagaming.com/realweather-f31334.html" >}}

{{< download name="Juggernaut" desc="Like in GTA V" image="https://i.imgur.com/m0QR3pF.jpg" download="https://www.gtagaming.com/juggernaut-f30941.html" >}}

{{< download name="Suicide Bomber" desc="Force pedestrians to hold a grenade" image="https://i.imgur.com/KpKlnDR.jpg" download="https://www.gtagaming.com/suicide-bomber-f30933.html" >}}

{{< download name="Ped Suicide V Style" desc="Force pedestrians to shoot themselves" image="https://i.imgur.com/ferNXrT.jpg" download="https://www.gtagaming.com/ped-suicide-v-style-f30912.html" >}}

{{< download name="Simple Hunger" desc="Lose health over time. Eat to regain health" image="https://i.imgur.com/DHTvrr1.jpg" download="https://www.gtagaming.com/simple-hunger-mod-f30907.html" >}}

{{< download name="Suicide V Style" desc="Shoot yourself" image="https://i.imgur.com/JMeifal.jpg" download="https://www.gtagaming.com/suicide-v-style-f30886.html" >}}

{{< download name="Stopwatch for Testing Cars" desc="Test car acceleration" image="https://i.imgur.com/uzRNtLm.jpg" download="https://www.gtagaming.com/stopwatch-for-testing-cars-f30882.html" >}}

{{< download name="Player Components Selector" desc="Hide body parts" image="https://i.imgur.com/PP61u54.jpg" download="https://www.gtagaming.com/player-components-selector-f30805.html" >}}

{{< download name="Weather Changer" desc="Change the weather" image="https://i.imgur.com/3h1OJ2l.jpg" download="https://www.gtagaming.com/weather-changer-f30696.html" >}}

{{< download name="Jesus Mode" desc="Walk on water" image="https://i.imgur.com/3QEwskG.jpg" download="https://www.gtagaming.com/jesus-mode-f30681.html" >}}

{{< download name="Timescale Selector" desc="Make game slower or faster" image="https://i.imgur.com/RpLcwNJ.jpg" download="https://www.gtagaming.com/timescale-selector-f30680.html" >}}

{{< download name="Universal Gravity Changer" desc="Change gravity for everything include cars (most mods don't affect vehicles)" image="https://i.imgur.com/qN6N7Fe.jpg" download="https://www.gtagaming.com/universal-gravity-changer-f30504.html" >}}

{{< download name="Player Model Selector" desc="Change your player model" image="https://i.imgur.com/rXjC3YK.jpg" download="https://www.gtagaming.com/player-model-selector-f30498.html" >}}

{{< download name="PermanentWeather" desc="Keep the weather the same" image="https://i.imgur.com/iw6g9OK.jpg" download="https://www.gtagaming.com/permanentweather-f30337.html" >}}

{{< download name="DxTory Crash Fix GUI" desc="Fix crash when ENB is enabled and DxTory running" image="https://i.imgur.com/zAV40q4.jpg" download="https://www.gtagaming.com/dxtory-crash-fix-gui-edition-f30072.html" >}}

{{< download name="Scare/Troll Mod" desc="Scare or troll your friends" image="https://i.imgur.com/EMYztX2.jpg" download="https://www.gtagaming.com/scare-troll-mod-f29467.html" >}}

{{< download name="Cruise Control" desc="Keep your speed the same" image="https://i.imgur.com/fjmSuIV.jpg" download="https://www.gtagaming.com/cruise-control-v1-1-f29463.html" >}}

{{< download name="Airbreak/Noclip" desc="Fly around" image="https://i.imgur.com/v57rR1n.jpg" download="https://www.gtagaming.com/open-source-airbreak-noclip-mod-f29375.html" >}}

{{< download name="Force Field++" desc="Put a forcefield around yourself" image="https://i.imgur.com/6m2ct87.jpg" download="https://www.gtagaming.com/force-field-f29330.html" >}}

{{< download name="Design Your Own Harlem Shake" desc="Sync dance animation and music" image="https://i.imgur.com/c6zzcST.jpg" download="https://www.gtagaming.com/design-your-own-harlem-shake-v1-1-f29040.html" >}}

{{< download name="Lap Timer" desc="Time your laps" image="https://i.imgur.com/h1OwwUR.jpg" download="https://www.gtagaming.com/lap-timer-mod-f28969.html" >}}

{{< download name="Stopwatch" desc="Time yourself" image="https://i.imgur.com/f2ncAE0.jpg" download="https://www.gtagaming.com/stopwatch-v1-1-f28931.html" >}}

{{< download name="Turn Signal Strobe Lights" desc="Turn signals flash" image="https://i.imgur.com/i3Coqfz.jpg" download="https://www.gtagaming.com/turn-signal-strobe-lights-v1-0-f28237.html" >}}

{{< download name="Strobe Lights" desc="Headlights flash" image="https://i.imgur.com/qtiiz6Z.jpg" download="https://www.gtagaming.com/strobe-lights-v1-0-f27955.html" >}}

{{< download name="Car Functions and Indicators" desc="Engine shutoff, cruise control, open doors, and check engine light. <a href=\"https://gist.github.com/Jitnaught/dbab08ce93be8766d16f1ce51dae67f6\">More info.</a>" image="https://i.imgur.com/Ulznmjg.jpg" download="https://mega.nz/#!tL4QyS4B!15evFN9t9ZTJMUV0wYoFIkn1C4o8BQMEzUsFYeNd2og" >}}

{{< download name="Check Engine Light" desc="Display when car is damaged. <a href=\"https://gist.github.com/Jitnaught/5a7daa3703d8aaba0e4a8b88d288cb2a\">More info.</a>" image="https://i.imgur.com/ZLupyuS.jpg" download="https://mega.nz/#!4Ch3VBrI!xmpEmkT7Cc9z6wB1-It-1wDJisHDNMaVXWngnSiyxGk" >}}

{{< download name="Cool Loop Ramp 2" desc="Another loop-de-loop made with Simple Native Trainer" image="https://i.imgur.com/IPXQJrJ.jpg" download="https://www.gtagaming.com/cool-loop-ramp-2-f23488.html" >}}

{{< download name="Cool Loop Ramp at Airport" desc="Loop-de-loop made with Simple Native Trainer" image="https://i.imgur.com/MdC5QMu.jpg" download="https://www.gtagaming.com/cool-loop-ramp-at-airport-f23461.html" >}}

## Other mods I've made

* Online Players Info: useless due to GFWL taken down
* GTA4-Mods Mod Approved Checked: useless due to GTA4-Mods taken down
* Vicious Trainer: trainer for use online, useless due to GFWL taken down
* Body Swap: broken mod, swap bodies with ped you're looking at
* Car Jump: useless
* Chuck Norris Mode: useless, basically God Mode
* Find A Car For Me: useless
* Headless: useless
* Jesus Mode (C++ version): broken
* Ragdoll: useless
