+++
title = 'My COD WAW bookmarks'
date = 2024-11-18T04:39:22-07:00
draft = false
+++

# My COD WAW documentation bookmarks

Could be outdated.

{{< bookmark title="Host server" link="https://steamcommunity.com/sharedfiles/filedetails/?id=351121917" desc="" >}}

{{< bookmark title="Useful tips and commands" link="https://steamcommunity.com/sharedfiles/filedetails/?id=351148618" desc="" >}}

{{< bookmark title="Custom zombies info" link="https://steamcommunity.com/groups/Imlikingmyzombies" desc="" >}}

{{< bookmark title="How to install and play custom zombies" link="https://steamcommunity.com/sharedfiles/filedetails/?id=113282307" desc="" >}}

{{< bookmark title="Modtools installation" link="https://confluence.ugx-mods.com/display/UGXMODS/Modtools+Installation+Guide" desc="" >}}

{{< bookmark title="COD 5 engine limitation" link="http://wiki.modsrepository.com/index.php?title=Call_of_Duty_5:_Engine_Limitation" desc="" >}}

{{< bookmark title="Enter devmap" link="http://www.zombiemodding.com/index.php?topic=16131.msg151559#msg151559" desc="" >}}

{{< bookmark title="Modding and mapping wiki" link="http://wiki.modsrepository.com/index.php?title=CODWAW" desc="" >}}

{{< bookmark title="UGX modtools" link="https://confluence.ugx-mods.com/display/UGXMODS/World+at+War+Modtools" desc="" >}}

{{< bookmark title="Mapping tutorials" link="http://modsonline.com/Tutorials-list-20.html" desc="" >}}

{{< bookmark title="Radiant for beginners tutorial" link="http://modsonline.com/Tutorials-read-485.html" desc="" >}}

{{< bookmark title="Creating a map tutorial" link="https://confluence.ugx-mods.com/display/UGXMODS/Creating+a+Map" desc="" >}}

{{< bookmark title="Import map/models" link="http://www.zombiemodding.com/index.php?topic=11029.0" desc="" >}}

{{< bookmark title="Downloaded models into radiant" link="http://www.zombiemodding.com/index.php?topic=15970.0" desc="" >}}

{{< bookmark title="Export blender model into WAW" link="http://www.3dmappers.com/kunena/=-tutorials-=-blender-3d/517-how-to-export-a-blender-model-into-waw" desc="" >}}

{{< bookmark title="Models in your map tutorial" link="http://modsonline.com/Tutorials-read-99.html" desc="" >}}

{{< bookmark title="Port source engine maps" link="http://ugx-mods.com/forum/index.php?topic=2997.0" desc="" >}}

{{< bookmark title="Create zombie and dog spawners tutorial" link="https://confluence.ugx-mods.com/display/UGXMODS/Creating+Zombie+and+Dog+Spawners" desc="" >}}

{{< bookmark title="Zones, sliding doors, spawns, and teleporters tutorial" link="http://www.zombiemodding.com/index.php?topic=5736.0" desc="" >}}

{{< bookmark title="Kino style teleporter" link="https://ugx-mods.com/forum/index.php?topic=2013.0" desc="" >}}

{{< bookmark title="Remove fog" link="http://www.zombiemodding.com/index.php?topic=15992.0" desc="" >}}

{{< bookmark title="Map crashing on startup, could not load rawfile" link="https://ugx-mods.com/forum/index.php?topic=9960.0" desc="" >}}

{{< bookmark title="Map run error, could not find script" link="http://www.zombiemodding.com/index.php?topic=21027.0" desc="" >}}

{{< bookmark title="fx_snow_blizzard_intense error" link="http://www.zombiemodding.com/index.php?topic=17295.0" desc="" >}}

{{< bookmark title="Red tint and sniperbolts tutorial text" link="http://www.zombiemodding.com/index.php?topic=16522.0" desc="" >}}

{{< bookmark title="Custom animations tutorial" link="https://www.youtube.com/watch?v=SnzkbDG7OLY" desc="" >}}

{{< bookmark title="Port COD4 guns tutorial" link="https://www.youtube.com/watch?v=6Sp97s9t11I" desc="" >}}

{{< bookmark title="Port Black Ops guns tutorial" link="https://www.youtube.com/watch?v=zrblKq1U-Qc" desc="" >}}

{{< bookmark title="Port Black Ops guns tutorial series" link="https://www.youtube.com/watch?v=_XjfkFfcP54" desc="" >}}

{{< bookmark title="Port Black Ops 2 guns tutorial" link="https://ugx-mods.com/forum/index.php?topic=646.0" desc="" >}}

{{< bookmark title="Port Black Ops 2 guns tutorial (video)" link="https://www.youtube.com/watch?v=LIROfMUsN9E" desc="" >}}

{{< bookmark title="Weapon porting guide" link="https://steamcommunity.com/sharedfiles/filedetails/?id=384822367" desc="" >}}
