+++
title = 'My RDR2 bookmarks'
date = 2024-11-18T04:39:11-07:00
draft = false
+++

# My RDR 2 documentation bookmarks

Could be outdated.

{{< bookmark title="RDR2Mods" link="https://www.rdr2mods.com/" desc="" >}}

{{< bookmark title="NativeDB" link="https://alloc8or.re/rdr3/nativedb/" desc="By alloc8or" >}}

{{< bookmark title="Native database" link="https://www.mod-rdr.com/nativedb/" desc="" >}}

{{< bookmark title="Modding wiki" link="https://www.mod-rdr.com/wiki/" desc="" >}}

{{< bookmark title="Player clothes" link="https://www.mod-rdr.com/wiki/clothes/" desc="" >}}

{{< bookmark title="Ped voices" link="https://www.mod-rdr.com/wiki/speech/" desc="" >}}

{{< bookmark title="Pickups" link="https://www.mod-rdr.com/wiki/pages/list-of-pickups-in-rdr2-r15/" desc="" >}}

{{< bookmark title="Scenarios" link="https://www.mod-rdr.com/wiki/pages/list-of-rdr2-scenarios/" desc="" >}}

{{< bookmark title="Weather types" link="https://www.mod-rdr.com/wiki/pages/list-of-weather-types-in-rdr2-r7/" desc="" >}}

{{< bookmark title="Weapon models" link="https://www.mod-rdr.com/wiki/pages/list-of-rdr2-weapon-models/" desc="" >}}

{{< bookmark title="Ammo types" link="https://www.mod-rdr.com/wiki/pages/list-of-rdr2-ammo-types/" desc="" >}}

{{< bookmark title="Ped models" link="https://www.mod-rdr.com/wiki/peds/" desc="" >}}

{{< bookmark title="Vehicle models" link="https://www.mod-rdr.com/wiki/pages/list-of-rdr2-vehicle-models/" desc="" >}}

{{< bookmark title="Object models" link="https://www.mod-rdr.com/wiki/pages/list-of-object-models-in-rdr2-r16/" desc="" >}}

{{< bookmark title="Speech" link="https://pastebin.com/P6349BEA" desc="" >}}

{{< bookmark title="Combat speech" link="https://pastebin.com/dn9zeUTD" desc="" >}}

{{< bookmark title="Animals" link="https://pastebin.com/Rhfjxdvc" desc="" >}}

{{< bookmark title="Factions" link="https://pastebin.com/6STsXCcT" desc="" >}}

{{< bookmark title="Peds" link="https://pastebin.com/cxXAJ1MV" desc="" >}}

{{< bookmark title="Weapons" link="https://pastebin.com/qjzGLdeB" desc="" >}}

{{< bookmark title="Male peds" link="https://pastebin.com/cKPkd583" desc="" >}}

{{< bookmark title="Female peds" link="https://pastebin.com/Y3VamWhh" desc="" >}}

{{< bookmark title="Horses" link="https://pastebin.com/EQJVdhDP" desc="" >}}

{{< bookmark title="Input" link="https://pastebin.com/uLhLmuvk" desc="" >}}
