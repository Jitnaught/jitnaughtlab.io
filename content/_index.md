+++
title = 'Jitnaught'
date = 2024-08-20T11:52:59-07:00
draft = false
+++

# About me

Programming since 2012. I've made mods for Minecraft, GTA IV, GTA V, Max Payne 3, RDR2, and Minetest. Created a few websites with Ruby on Rails. Created [an animation](https://www.youtube.com/watch?v=ekphJoq_41c) using Godot.

[My GTA IV mods](/gta4mods)

[My Max Payne 3 mods](/mp3mods)

[My websites](/sites)

[My games](/games)

[My GTA 5 documentation bookmarks](/gta5docs)

[My GTA 4 documentation bookmarks](/gta4docs)

[My RDR 2 documentation bookmarks](/rdr2docs)

[My COD WAW documentation bookmarks](/wawdocs)
